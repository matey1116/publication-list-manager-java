package com.example.jwt.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;
import lombok.ToString;

import java.util.List;
import java.util.Map;

@Data
@ToString
@JacksonXmlRootElement(localName = "article")
public class QueryEntity {
//    @JsonIgnore
    private String _id;
    @JsonIgnore
    private String userID;
    private List<String> authors;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String title;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String Abstract;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String venue;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String volume;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String number;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String pages;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String year;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String type;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String key;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String doi;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String ee;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String url;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String publisher;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String booktitle;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, String> metadata;
}
