package com.example.jwt.model;

import lombok.Data;

import java.util.List;

@Data
public class ShareArticlesEntity {
    private String _id;
    private String userID;
    private List<String> articles;
}
