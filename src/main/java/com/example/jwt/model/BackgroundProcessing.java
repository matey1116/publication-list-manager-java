package com.example.jwt.model;

import com.example.jwt.data.QueryResponse;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class BackgroundProcessing {
    @Id
    private String id;
    private String uuid;
    private Integer progress;
    private QueryResponse queryResponse;
}
