package com.example.jwt.model;


import lombok.Data;
import org.springframework.data.annotation.Id;


@Data
public class UserEntity {

    @Id
    private String id;

    private String email;

    private String password;

    private String firstname;

    private String lastname;

    private int access;

    private String secret;

    private boolean twoFactorAuth;

    private boolean activated;

    private String activationUUID;

    private String resetUUID;

    private boolean confirmProviders;

//    private boolean mendeleyEnabled;
//    private String mendeleyAuth;
//    private Date mendeleyExpire;
//    private String mendeleyRefresh;

    public void set2fa(boolean twoFactorAuth){
        this.twoFactorAuth = twoFactorAuth;

    }
    public boolean getConfirmProviders(){
        return confirmProviders;
    }
//    public boolean getMendeleyEnabled(){
//        return mendeleyEnabled;
//    }
    public boolean is2faEnabled(){
        return twoFactorAuth;
    }
}
