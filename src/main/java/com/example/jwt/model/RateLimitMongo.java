package com.example.jwt.model;

import lombok.Data;
import org.springframework.data.annotation.Id;


@Data
public class RateLimitMongo {
    @Id
    private String id;
    private int hitCount;
}
