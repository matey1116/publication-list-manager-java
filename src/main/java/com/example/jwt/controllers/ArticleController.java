package com.example.jwt.controllers;

import com.example.jwt.data.ExportData;
import com.example.jwt.data.ImportData;
import com.example.jwt.exceptions.Exception400;
import com.example.jwt.exceptions.Exception500;
import com.example.jwt.model.QueryEntity;
import com.example.jwt.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;

@RestController
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    ArticleService articleService;

    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public ResponseEntity<?> importArticle(@Valid @NotNull @RequestBody ImportData importData) throws Exception400, Exception500 {
        return new ResponseEntity<>(articleService.importArticle(importData), HttpStatus.OK);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<?> getarticles() throws Exception400 {
        return new ResponseEntity<>(articleService.getarticles(), HttpStatus.OK);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getarticles(@Valid @NotNull @NotEmpty @PathVariable String id) throws Exception400 {
        return new ResponseEntity<>(articleService.getarticle(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/export", method = RequestMethod.POST)
    public ResponseEntity<?> export(@Valid @NotNull @RequestBody ExportData exportData) throws Exception400, IOException {
        if (exportData.getExportTo().equalsIgnoreCase("xml")) {
            return ResponseEntity.ok().contentType(MediaType.TEXT_XML).body(articleService.export(exportData));
        }else if(exportData.getExportTo().equalsIgnoreCase("csv")){
            return ResponseEntity.ok().contentType(MediaType.TEXT_PLAIN).body(articleService.export(exportData));
        }
        else
            return new ResponseEntity<>(articleService.export(exportData), HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteArticle(@Valid @NotNull @NotEmpty @PathVariable String id) {
        return new ResponseEntity<>(articleService.delete(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/put/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> modifyArticle(@Valid @NotNull @RequestBody QueryEntity queryEntity, @Valid @NotNull @PathVariable String id) throws Exception400, NoSuchMethodException {
        return new ResponseEntity<>(articleService.modify(queryEntity, id), HttpStatus.OK);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<?> addArticle(@Valid @NotNull @RequestBody QueryEntity queryEntity) throws Exception400 {
        return new ResponseEntity<>(articleService.add(queryEntity), HttpStatus.OK);
    }
}
