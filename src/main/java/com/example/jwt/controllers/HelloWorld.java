package com.example.jwt.controllers;

import com.example.jwt.exceptions.Exception400;
import com.example.jwt.exceptions.Exception500;
import com.example.jwt.repository.QueryRepository;
import com.example.jwt.repository.UserRepository;
import com.example.jwt.services.ArXivService;
import com.example.jwt.services.DBLPService;
import com.example.jwt.services.ScopusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;

@RestController
public class HelloWorld {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    DBLPService dblpService;
    @Autowired
    QueryRepository queryRepository;

    @Autowired
    ScopusService scopusService;

    @Autowired
    ArXivService arXivService;

    @GetMapping("/hello")
    public Object helloWorld() throws UnsupportedEncodingException, Exception500, Exception400 {
//        return arXivService.getArXivForArticle("Median-of-k Jumplists and Dangling-Min BSTs.");
        return queryRepository.findAll();
    }

}
