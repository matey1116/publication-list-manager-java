package com.example.jwt.controllers;

import com.example.jwt.data.QueryRequest;
import com.example.jwt.data.QueryResponse;
import com.example.jwt.exceptions.Exception400;
import com.example.jwt.exceptions.Exception500;
import com.example.jwt.model.QueryEntity;
import com.example.jwt.services.DBLPService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
@RequestMapping("/query")
@Slf4j
public class QueryController {

    @Autowired
    private DBLPService dblpService;

    @PostMapping("/dblp")
    public Object dblpFetch(@Valid @RequestBody QueryRequest request) throws UnsupportedEncodingException, Exception500, Exception400 {
        return dblpService.fetchDBLP(request);
    }

    @PostMapping("/dblp/save")
    public List<QueryEntity> dblpSave(@Valid @RequestBody QueryResponse request){
        return dblpService.saveDBLP(request);
    }

    @GetMapping("/status/{uuid}")
    public Object getStatus(@Valid @NotNull @PathVariable String uuid) throws Exception400 {
        return dblpService.getStatus(uuid);
    }
}
