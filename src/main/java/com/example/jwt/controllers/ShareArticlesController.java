package com.example.jwt.controllers;

import com.example.jwt.exceptions.Exception400;
import com.example.jwt.services.ShareArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/share")
public class ShareArticlesController {
    @Autowired
    ShareArticlesService shareArticlesService;

    @RequestMapping(value = "/generate", method = RequestMethod.POST)
    public ResponseEntity<?> generatelist(@Valid @NotEmpty(message = "Test") @RequestBody List<String> articles) throws Exception400 {
        return new ResponseEntity<>(shareArticlesService.generateList(articles), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getID(@Valid @NotNull @PathVariable String id) throws Exception400 {
        return new ResponseEntity<>(shareArticlesService.getID(id), HttpStatus.OK);
    }
}
