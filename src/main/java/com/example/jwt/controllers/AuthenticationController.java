package com.example.jwt.controllers;

import com.example.jwt.data.*;
import com.example.jwt.exceptions.Exception400;
import com.example.jwt.services.AccountService;
import io.github.bucket4j.Bucket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

@RestController
@RequestMapping("/account")
@Validated
public class AuthenticationController {
    @Autowired
    AccountService accountService;

    private Bucket bucket;

    public AuthenticationController() {
//        Bandwidth limit = Bandwidth.classic(1000, Refill.greedy(1000, Duration.ofMinutes(1)));
//        this.bucket = Bucket4j.builder()
//                .addLimit(limit)
//                .build();

    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@Valid @RequestBody LoginData authenticationRequest, HttpServletResponse response) throws Exception {
        String[] serviceResponse = accountService.login(authenticationRequest);
        if (serviceResponse.length == 2) {
            response.addHeader("Set-Cookie", serviceResponse[1]);
        }
        return new ResponseEntity<>(new JwtResponseData(serviceResponse[0]), HttpStatus.OK);
    }

    @RequestMapping(value = "/2fa", method = RequestMethod.POST)
    public ResponseEntity<?> factorAuthentication(@Valid @RequestBody TwoFactorData twoFactorData) throws Exception {
        return new ResponseEntity<>(accountService.twoFactorAuth(twoFactorData), HttpStatus.OK);
    }

    @RequestMapping(value = "/generateQR/{uuid}", method = RequestMethod.GET)
    public ResponseEntity<?> generateQR(@Valid @NotEmpty(message = "UUID Missing") @PathVariable String uuid) throws Exception {
        return new ResponseEntity<>(accountService.generateQR(uuid), HttpStatus.OK);
    }

    @RequestMapping(value = "/activate/{uuid}", method = RequestMethod.GET)
    public ResponseEntity<?> activate(@Valid @NotEmpty(message = "UUID missing") @PathVariable String uuid) throws Exception400 {
        return new ResponseEntity<>(accountService.activate(uuid), HttpStatus.OK);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@Valid @RequestBody RegisterData registerData) throws Exception {
        return new ResponseEntity<>(accountService.register(registerData), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/change", method = RequestMethod.POST)
    public ResponseEntity<?> changePassword(@Valid @RequestBody ChangePasswordData changePasswordData) throws Exception {
        return new ResponseEntity<>(accountService.changePassword(changePasswordData), HttpStatus.OK);
    }

    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public ResponseEntity<?> resetPassword(@Valid @RequestBody PasswordResetBody passwordResetBody) throws Exception400 {
        return new ResponseEntity<>(accountService.resetPassword(passwordResetBody), HttpStatus.OK);
    }

    @RequestMapping(value = "/providers", method = RequestMethod.GET)
    public ResponseEntity<?> getProviders() throws Exception400 {
        return new ResponseEntity<>(accountService.getProviders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/prompt", method = RequestMethod.POST)
    public ResponseEntity<?> postPrompt() throws Exception400 {
        return new ResponseEntity<>(accountService.postPrompt(), HttpStatus.OK);
    }

    @RequestMapping(value = "/is2FAenabled", method = RequestMethod.GET)
    public ResponseEntity<?> is2FAEnabled() throws Exception400 {
        return new ResponseEntity<>(accountService.is2FAEnabled(), HttpStatus.OK);
    }

}
