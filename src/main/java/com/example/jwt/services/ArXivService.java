package com.example.jwt.services;

import com.example.jwt.data.arxiv.ArXivData;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Service
public class ArXivService {
    private final RestTemplate restTemplate;
    private final static String titleURL = "http://export.arxiv.org/api/query?search_query=ti:{articleTitle}&max_results=20";

    private final static  String idURL = "http://export.arxiv.org/api/query?search_query=id:{ID}";

    private final static String authorURL = "http://export.arxiv.org/api/query?search_query=au:{AU}&max_resutls=100";


    public ArXivService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public ArXivData getArXivForArticle(String titleName) throws UnsupportedEncodingException {
        return restTemplate.getForObject(titleURL, ArXivData.class, URLEncoder.encode(titleName, StandardCharsets.UTF_8.toString()));
    }

    public ArXivData getArxivForID(String ID) throws UnsupportedEncodingException {
        return restTemplate.getForObject(idURL, ArXivData.class, URLEncoder.encode(ID, StandardCharsets.UTF_8.toString()));
    }

    public ArXivData getArxivForAuthor(String author) throws UnsupportedEncodingException {
        return restTemplate.getForObject(authorURL, ArXivData.class, URLEncoder.encode(author, StandardCharsets.UTF_8.toString()));
    }

}
