package com.example.jwt.services;

import com.example.jwt.data.CustomUserDetails;
import com.example.jwt.exceptions.Exception400;
import com.example.jwt.model.QueryEntity;
import com.example.jwt.model.ShareArticlesEntity;
import com.example.jwt.repository.QueryRepository;
import com.example.jwt.repository.ShareArticlesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.security.RolesAllowed;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ShareArticlesService {
    @Autowired
    ShareArticlesRepository shareArticlesRepository;
    @Autowired
    QueryRepository queryRepository;

    @RolesAllowed("ROLE_USER")
    public String generateList(List<String> articles) throws Exception400 {
        String userID = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();

        Optional<ShareArticlesEntity> shareArticlesEntities = shareArticlesRepository.findArticles(articles, userID);
        if(shareArticlesEntities.isPresent()){
            return shareArticlesEntities.get().get_id();
        }

        List<QueryEntity> queryEntityList = queryRepository.findObjectsByObjectIDs(new HashSet<>(articles), userID).get();
        if(queryEntityList.size() == 0 ){
            throw new Exception400("articles", "Invalid articles list");
        }
        ShareArticlesEntity shareArticlesEntity = new ShareArticlesEntity();
        shareArticlesEntity.setUserID(userID);
        shareArticlesEntity.setArticles(queryEntityList.stream().map(QueryEntity::get_id).collect(Collectors.toUnmodifiableList()));
        return shareArticlesRepository.save(shareArticlesEntity).get_id();
    }

    public Object getID(String id) throws Exception400 {
        Optional<ShareArticlesEntity> shareArticlesEntity = shareArticlesRepository.findById(id);
        if(shareArticlesEntity.isEmpty()) throw new Exception400("id", "Share ID is invalid");
        return queryRepository.findByQueryIds(shareArticlesEntity.get().getArticles()).get();
    }
}
