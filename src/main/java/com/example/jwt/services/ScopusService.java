package com.example.jwt.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;


@Service
public class ScopusService {
    private final RestTemplate restTemplate;


    @Autowired
    public ScopusService(RestTemplateBuilder restTemplateBuilder){
        this.restTemplate = restTemplateBuilder.build();
    }

    public Object test(){
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-ELS-APIKey", "51226f2971ef215331a440f2e8e11c5b");
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>("parameters",headers);
        return restTemplate.exchange("https://api.elsevier.com/authenticate/", HttpMethod.GET, entity, Object.class);
    }

//    public String fetchAbstractByDoi(String doi){
//        return
//    }
}
