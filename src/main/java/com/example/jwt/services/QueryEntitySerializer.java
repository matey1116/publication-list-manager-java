package com.example.jwt.services;

import com.example.jwt.model.QueryEntity;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;

import java.io.IOException;

public class QueryEntitySerializer extends StdSerializer<QueryEntity> {
    public QueryEntitySerializer() { this(null); }

    public QueryEntitySerializer(Class<QueryEntity> t) {
        super(t);
    }

    @Override
    public void serialize(QueryEntity queryEntity, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        final ToXmlGenerator toXmlGenerator = (ToXmlGenerator) jsonGenerator;

        toXmlGenerator.writeStartObject();
        toXmlGenerator.writeStringField("title", queryEntity.getTitle());
        toXmlGenerator.writeFieldName("authors");
        toXmlGenerator.writeStartObject();
        for (String a : queryEntity.getAuthors()) {
            toXmlGenerator.writeStringField("author", a);
        }
        toXmlGenerator.writeEndObject();
    }
}
