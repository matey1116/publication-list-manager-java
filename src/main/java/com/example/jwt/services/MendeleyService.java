//package com.example.jwt.services;
//
//import com.example.jwt.data.CustomUserDetails;
//import com.example.jwt.data.QueryRequest;
//import com.example.jwt.data.mendeley.MendeleyEntry;
//import com.example.jwt.exceptions.Exception400;
//import com.example.jwt.exceptions.Exception500;
//import com.example.jwt.model.UserEntity;
//import com.example.jwt.repository.UserRepository;
//import lombok.Data;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.codec.binary.Base64;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.web.client.RestTemplateBuilder;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.MediaType;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.stereotype.Service;
//import org.springframework.util.LinkedMultiValueMap;
//import org.springframework.util.MultiValueMap;
//import org.springframework.web.client.RestTemplate;
//
//import javax.annotation.security.RolesAllowed;
//import java.io.UnsupportedEncodingException;
//import java.net.URLEncoder;
//import java.nio.charset.StandardCharsets;
//import java.util.Calendar;
//import java.util.Date;
//
//@Service
//@Slf4j
//public class MendeleyService {
//
//    @Autowired
//    UserRepository userRepository;
//
//    @Value("${redirect.url}")
//    private String redirectURL;
//
//    private RestTemplate restTemplate;
//
//
//    public MendeleyService(RestTemplateBuilder restTemplateBuilder) {
//        this.restTemplate = restTemplateBuilder.build();
//    }
//
//    private final String mendeleyTokenURL = "https://api.mendeley.com/oauth/token";
//    private final String catalog = "https://api.mendeley.com/search/catalog";
//    private final String ownDocuments = "https://api.mendeley.com/documents";
//
//    private final HttpHeaders httpHeaders = new HttpHeaders();
//
//    public Object mendeleyAuthentication(String state, String code) throws Exception500 {
//        String auth = "9055" + ":" + "hbkR9vl1gBjEc3nB";
//        byte[] encodedAuth = Base64.encodeBase64(
//                auth.getBytes(StandardCharsets.US_ASCII));
//        String authHeader = "Basic " + new String(encodedAuth);
//        httpHeaders.set("Authorization", authHeader);
//        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
//        map.add("grant_type", "authorization_code");
//        map.add("code", code);
//        map.add("redirect_uri", redirectURL + "/account/mendeley");
//        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, httpHeaders);
//
//        try {
//            MendeleyResponse response = restTemplate.exchange(mendeleyTokenURL, HttpMethod.POST, entity, MendeleyResponse.class).getBody();
//            if (response != null) {
//                log.info(response.getAccess_token());
//                log.info(response.getRefresh_token());
//                UserEntity userEntity = userRepository.findById(state).orElseThrow(() -> new Exception400("user", "Not Found"));
//                Calendar calendar = Calendar.getInstance();
//                calendar.add(Calendar.SECOND, response.getExpires_in());
//                userEntity.setMendeleyExpire(calendar.getTime());
//                userEntity.setMendeleyRefresh(response.getRefresh_token());
//                userEntity.setMendeleyAuth(response.getAccess_token());
//                userRepository.save(userEntity);
//                return response.getAccess_token();
//            } else {
//                throw new Exception500("Exception");
//            }
//        } catch (Exception e) {
//            throw new Exception500(e.getMessage());
//        }
//    }
//
//
//
//    public String refreshToken(String refreshToken) throws Exception500 {
//        String auth = "9055" + ":" + "hbkR9vl1gBjEc3nB";
//        byte[] encodedAuth = Base64.encodeBase64(
//                auth.getBytes(StandardCharsets.US_ASCII));
//        String authHeader = "Basic " + new String(encodedAuth);
//        httpHeaders.set("Authorization", authHeader);
//
//        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
//        map.add("grant_type", "refresh_token");
//        map.add("refresh_token", refreshToken);
//        map.add("redirect_uri", redirectURL + "/account/mendeley");
//        System.out.println(map);
//        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, httpHeaders);
//
//        try {
//            MendeleyResponse response = restTemplate.exchange(mendeleyTokenURL, HttpMethod.POST, entity, MendeleyResponse.class).getBody();
//            if (response != null) {
//                return response.getAccess_token();
//            } else throw new Exception500("Unexpected error occurred");
//        } catch (Exception e) {
//            throw new Exception500(e.getMessage());
//        }
//    }
//
//    @RolesAllowed("ROLE_USER")
//    public Object fetch(QueryRequest queryRequest) throws Exception400, Exception500, UnsupportedEncodingException {
//        httpHeaders.clear();
//        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        UserEntity userEntity = userRepository.findById(user.getId()).orElseThrow(() -> new Exception400("User", "User not found"));
//
//        if (userEntity.getMendeleyExpire().after(new Date())) {
//            log.info("Date is in the future " + userEntity.getMendeleyExpire().toString());
//        } else {
//            log.info("Date is in the past " + userEntity.getMendeleyExpire().toString());
//            userEntity.setMendeleyAuth(refreshToken(userEntity.getMendeleyRefresh()));
//            Calendar calendar = Calendar.getInstance();
//            calendar.add(Calendar.SECOND, 3550);
//            userEntity.setMendeleyExpire(calendar.getTime());
//            userRepository.save(userEntity);
//        }
//
//        if (queryRequest.isOwnRecord() || (user.getFirstName() + " " + user.getLastName()).equalsIgnoreCase(queryRequest.getName())) {
//            httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//            httpHeaders.set("Authorization", "Bearer " + userEntity.getMendeleyAuth());
//            httpHeaders.set("Accept", "application/vnd.mendeley-document.1+json");
//            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(httpHeaders);
//
//            return restTemplate.exchange(ownDocuments, HttpMethod.GET, entity, MendeleyEntry[].class);
////            return restTemplate.exchange(catalog + "?query=" + URLEncoder.encode(queryRequest.getName(), StandardCharsets.UTF_8.toString()) + "&sortBy=relevance", HttpMethod.GET, entity, Object.class);
//        } else {
//            if (queryRequest.queryAuthor()) {
//                httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//                httpHeaders.set("Authorization", "Bearer " + userEntity.getMendeleyAuth());
//                httpHeaders.set("Accept", "application/vnd.mendeley-document.1+json");
//                HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(httpHeaders);
//
//                Object object = restTemplate.exchange(catalog + "?author=" + URLEncoder.encode(queryRequest.getName(), StandardCharsets.UTF_8.toString()) + "&limit=100", HttpMethod.GET, entity, Object.class);
//                return object;
//            } else {
//                httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//                httpHeaders.set("Authorization", "Bearer " + userEntity.getMendeleyAuth());
//                httpHeaders.set("Accept", "application/vnd.mendeley-document.1+json");
//                HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(httpHeaders);
//
//                Object object = restTemplate.exchange(catalog + "?title=" + URLEncoder.encode(queryRequest.getName(), StandardCharsets.UTF_8.toString()) + "&limit=100", HttpMethod.GET, entity, Object.class);
//                return object;
//            }
//        }
//    }
//}
//
//@Data
//class MendeleyResponse {
//    private String access_token;
//    private String refresh_token;
//    private Integer expires_in;
//}
