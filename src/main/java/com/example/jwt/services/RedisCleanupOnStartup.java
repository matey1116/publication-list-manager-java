package com.example.jwt.services;

import com.example.jwt.model.RateLimitMongo;
import com.example.jwt.repository.RateLimitMongoRepository;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class RedisCleanupOnStartup implements ApplicationListener<ApplicationReadyEvent> {
    @Autowired
    RateLimitMongoRepository rateLimitMongoRepository;


    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        rateLimitMongoRepository.findAll().forEach(entity -> {
            entity.setHitCount(10);
            rateLimitMongoRepository.save(entity);
        });
    }
}
