package com.example.jwt.services;

import com.example.jwt.data.CustomUserDetails;
import com.example.jwt.model.UserEntity;
import com.example.jwt.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@Slf4j
public class JwtUserDetailService implements UserDetailsService {
    private final UserRepository userRepository;

    private final PasswordEncoder bcryptEncoder;


    @Autowired
    public JwtUserDetailService(UserRepository userRepository, PasswordEncoder bcryptEncoder) {
        this.userRepository = userRepository;
        this.bcryptEncoder = bcryptEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        UserEntity user = userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("User not found with email: " + email));

        if(!user.isActivated()){
            throw new DisabledException("User is not activated");
        }

        return buildUserForAuthentication(user);

    }

    private Collection<GrantedAuthority> getAuthorities(Integer access){
        return AuthorityUtils.createAuthorityList(access == 1 ? "ADMIN": "USER");
    }

    private User buildUserForAuthentication(UserEntity user) {
        String email = user.getEmail();
        String password = user.getPassword();
        int access = user.getAccess();
        String id = user.getId();
        String firstName = user.getFirstname();
        String lastName = user.getLastname();

        return new CustomUserDetails(id, email, password, firstName, lastName, access, getAuthorities(user.getAccess()));
    }
}
