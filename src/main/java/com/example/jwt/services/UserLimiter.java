package com.example.jwt.services;

import com.example.jwt.model.RateLimitMongo;
import com.example.jwt.repository.RateLimitMongoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class UserLimiter {

    @Autowired
    RateLimitMongoRepository rateLimitMongoRepository;

    public boolean checkLimit(String id){
        RateLimitMongo rateLimitMongo = rateLimitMongoRepository.findById(id).orElse(null);
        if(rateLimitMongo == null){
            rateLimitMongo = new RateLimitMongo();
            rateLimitMongo.setHitCount(9);
            rateLimitMongo.setId(id);
            rateLimitMongoRepository.save(rateLimitMongo);
            return true;
        }
        else{
            if(rateLimitMongo.getHitCount() > 0){
                rateLimitMongo.setHitCount(rateLimitMongo.getHitCount() - 1);
                rateLimitMongoRepository.save(rateLimitMongo);
                return true;
            }
            else{
                return false;
            }
        }
    }

    @Scheduled(fixedRate = 3600000)
    public void resetHitCount(){
        rateLimitMongoRepository.findAll().forEach(entity -> {
            entity.setHitCount(10);
            rateLimitMongoRepository.save(entity);
        });
    }
}
