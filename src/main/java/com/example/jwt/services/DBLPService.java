package com.example.jwt.services;

import com.example.jwt.data.CustomUserDetails;
import com.example.jwt.data.QueryRequest;
import com.example.jwt.data.QueryResponse;
import com.example.jwt.data.arxiv.ArXivData;
import com.example.jwt.data.arxiv.ArXivEntry;
import com.example.jwt.data.dblp.DBLPArticleData;
import com.example.jwt.data.dblp.DBLPHit;
import com.example.jwt.data.dblp.DBLPRawData;
import com.example.jwt.exceptions.Exception400;
import com.example.jwt.exceptions.Exception500;
import com.example.jwt.model.BackgroundProcessing;
import com.example.jwt.model.QueryEntity;
import com.example.jwt.repository.BackgroundProcessingRepository;
import com.example.jwt.repository.QueryRepository;
import lombok.extern.slf4j.Slf4j;
import org.jobrunr.jobs.context.JobContext;
import org.jobrunr.scheduling.JobScheduler;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.security.RolesAllowed;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.AbstractMap;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Service
@Slf4j
public class DBLPService {

    @Autowired
    QueryRepository queryRepository;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    ArXivService arXivService;

    @Autowired
    JobScheduler jobScheduler;

    @Autowired
    BackgroundProcessingRepository backgroundProcessingRepository;

    @Autowired
    MongoTemplate mongoTemplate;

    private final RestTemplate restTemplate;

    @Autowired
    public DBLPService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @RolesAllowed("ROLE_USER")
    public List<QueryEntity> saveDBLP(QueryResponse queryResponse) {
        String userID = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        List<String> titles = queryRepository.findByUserID(userID).get().stream().map(QueryEntity::getTitle).collect(Collectors.toUnmodifiableList());
        return queryRepository.saveAll(queryResponse.getDblpArticles().stream()
                .filter(dblpArticleData -> titles.stream().noneMatch(t -> t.trim().toLowerCase().contains(dblpArticleData.getTitle().trim().toLowerCase())))
                .map(article -> {
                    QueryEntity entity = modelMapper.map(article, QueryEntity.class);
                    entity.setUserID(userID);
                    entity.setAbstract(article.getAbstract());
                    return entity;
                }).collect(Collectors.toList()));
    }

    //    @Cacheable("queryresponse")
    public Object fetchDBLP(QueryRequest queryRequest) throws UnsupportedEncodingException, Exception400, Exception500 {
        Object result = getDBLPRemote(queryRequest);
        if (result instanceof QueryResponse) return result;
        else {
            return new AbstractMap.SimpleEntry<>("uuid", result.toString());
        }
    }

    private Object getDBLPRemote(QueryRequest queryRequest) throws UnsupportedEncodingException, Exception400, Exception500 {
        QueryResponse responseData = new QueryResponse();
        try {
            String url = "https://dblp.org/search/publ/api?h=1000&q=";
            //fetch abstracts from DBLP
            DBLPRawData dblpRawData = restTemplate.getForObject(url + URLEncoder.encode(queryRequest.getName(), StandardCharsets.UTF_8.toString()), DBLPRawData.class);

            //check if any results were retrieved
            if (dblpRawData != null && !dblpRawData.getHits().isEmpty()) {
                //if searching by author
                if (queryRequest.queryAuthor()) {
                    //filter out articles that are not by the author
                    responseData.setDblpArticles(
                            //custom filtering using java streams to filter only articles by the requested author
                            dblpRawData.getHits().stream().filter(hit -> hit.getInfo().getAuthors() != null && hit.getInfo().getAuthors().stream().map(String::toLowerCase).collect(Collectors.toUnmodifiableList()).contains(queryRequest.getName().toLowerCase()))
                                    .map(DBLPHit::getInfo).collect(Collectors.toList()));
                    //check if after filtering articles are left
                    if (!responseData.getDblpArticles().isEmpty()) {
                        //if user requests abstracts queue the job else return the resutls
                        return queryRequest.getAbstracts() ? jobScheduler.enqueue(() -> getAbstracts(responseData, JobContext.Null)) : responseData;
                    } else throw new Exception400(queryRequest.getType(), "No articles for the given author");
                }
                //if searching a specific article title
                else {
                    //filter
                    responseData.setDblpArticles(
                            //custom filtering using java streams to filter only articles containing the name
                            dblpRawData.getHits().stream().filter(hit -> hit.getInfo().getTitle() != null && hit.getInfo().getTitle().toLowerCase().contains(queryRequest.getName().toLowerCase()))
                                    .map(DBLPHit::getInfo).collect(Collectors.toList()));
                    if (!responseData.getDblpArticles().isEmpty()) {
                        //if abstracts are requested, schedule the task, else return data
                        return queryRequest.getAbstracts() ? jobScheduler.enqueue(() -> getAbstracts(responseData, JobContext.Null)) : responseData;
                    } else throw new Exception400(queryRequest.getType(), "No articles for the given title");

                }
            } else throw new Exception500("Unexpected error");
        } catch (Exception e) {
            if (e.getMessage().contains("Error while extracting response for type [class com.example.jwt.data.dblp.DBLPRawData]"))
                throw new Exception400(queryRequest.getType(), queryRequest.getType().equalsIgnoreCase("author") ? "No articles for the given author" : "No articles for the given title");
            else if (e instanceof Exception400) {
                throw e;
            } else {
                throw new Exception500(e.getMessage());
            }
        }
    }

    public void getAbstracts(QueryResponse responseData, JobContext jobContext) throws UnsupportedEncodingException {
        //create a new background processing job
        BackgroundProcessing backgroundProcessing = new BackgroundProcessing();
        backgroundProcessing.setUuid(jobContext.getJobId().toString());
        backgroundProcessing.setProgress(0);
        String id = backgroundProcessingRepository.save(backgroundProcessing).getId();
        int size = responseData.getDblpArticles().size();
        ArXivData arXivData;
        //start going over all the articles
        for (int i = 0; i < size; i++) {
            DBLPArticleData dblpArticleData = responseData.getDblpArticles().get(i);
            //check if article has arxiv ID
            if (dblpArticleData.getEe() != null && dblpArticleData.getEe().contains("arxiv.org"))
                //retrieve arxiv data by ID
                arXivData = arXivService.getArxivForID(dblpArticleData.getEe().split("https?://arxiv.org/abs/")[1]);
            else {
                //retrieve 100 arxiv articles containing by a given article title (search is wildcard)
                arXivData = arXivService.getArXivForArticle(dblpArticleData.getTitle());

                //check if arxiv returned data
                if (arXivData != null && arXivData.getEntry() != null && !arXivData.getEntry().isEmpty())
                    //filter results
                    arXivData.setEntry(arXivData.getEntry().stream().
                            //custom filter only keeping articles that have Levenshtein title distance against the searched title of less than 10
                            filter(arXivEntry -> Levenshtein.distance(arXivEntry.getTitle(), dblpArticleData.getTitle()) < 10).collect(Collectors.toUnmodifiableList()));
            }
            //check if an entry exists after filtering
            if (arXivData != null && arXivData.getEntry() != null && !arXivData.getEntry().isEmpty()) {
                //if it does set update the doi if one is present and set abstract
                ArXivEntry arXivEntry = arXivData.getEntry().get(0);
                if (arXivEntry.getDoi() != null) dblpArticleData.setDoi(arXivEntry.getDoi());
                if (arXivEntry.getSummary() != null) dblpArticleData.setAbstract(arXivEntry.getSummary());
            }
            log.info("Job ID " + jobContext.getJobId().toString() + " --- " + (i * 99) / size);
            //update database progress inorder the user to see the progress
            updateProgress(id, (i * 99) / size);
        }
        //when processing finishes set progress to 100 and save the processed data to the database
        backgroundProcessing.setProgress(100);
        backgroundProcessing.setQueryResponse(responseData);
        backgroundProcessingRepository.save(backgroundProcessing);
    }

    public void updateProgress(String id, Integer progress) {
        Query query = new Query().addCriteria(where("_id").is(id));

        Update update = new Update();
        update.set("progress", progress);
        mongoTemplate.update(BackgroundProcessing.class).matching(query).apply(update).first();
    }

    public Object getStatus(String uuid) throws Exception400 {
        BackgroundProcessing backgroundProcessing = backgroundProcessingRepository.findByUuid(uuid).orElseThrow(() -> new Exception400("uuid", "Not Found"));
        if (backgroundProcessing.getProgress() != 100)
            return new AbstractMap.SimpleEntry<>("progress", backgroundProcessing.getProgress());
        else {
            backgroundProcessingRepository.deleteById(backgroundProcessing.getId());
            return new AbstractMap.SimpleEntry<>("response", backgroundProcessing.getQueryResponse());
        }
    }
}
