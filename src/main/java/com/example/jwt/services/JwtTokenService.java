package com.example.jwt.services;

import com.example.jwt.data.CustomUserDetails;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtTokenService {

    @Value("${jwt.secret}")
    private String secret;// = "S3CrET";

    @Autowired
    SecureRandom secureRandom;

    @Autowired
    MessageDigest digest256;

    public static final long JWT_TOKEN_VALIDITY = 30 * 24 * 60 * 60;

    //retrieve username from jwt token
    public String getEmailFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    //retrieve expiration date from jwt token
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    //for retrieving any information from token we will need the secret key
    public Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    //check if the token has expired
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    private String fingerprintCookie() {
        //token security using https://cheatsheetseries.owasp.org/cheatsheets/JSON_Web_Token_for_Java_Cheat_Sheet.html#token-sidejacking
        byte[] randomFgp = new byte[50];
        secureRandom.nextBytes(randomFgp);
        return DatatypeConverter.printHexBinary(randomFgp);

    }

    //generate token for user
    public String[] generateToken(Authentication authentication) throws UnsupportedEncodingException {

        String userFingerprint = fingerprintCookie();
        String fingerprintCookie = "__Secure-Fgp=" + userFingerprint
                + "; SameSite=None; HttpOnly; Secure; domain=api.publicationlistmanager.me; path=/";

        byte[] userFingerprintDigest = digest256.digest(userFingerprint.getBytes(StandardCharsets.UTF_8));
        String userFingerprintHash = DatatypeConverter.printHexBinary(userFingerprintDigest);


        final Map<String, Object> claims = new HashMap<>();
        final CustomUserDetails user = (CustomUserDetails) authentication.getPrincipal();

        claims.put("access", user.getAccess());
        claims.put("id", user.getId());
        claims.put("firstName", user.getFirstName());
        claims.put("lastName", user.getLastName());
        claims.put("userFingerprint", userFingerprintHash);
        return new String[]{generateToken(claims, user.getEmail()), fingerprintCookie};
    }

    //while creating the token -
    //1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
    //2. Sign the JWT using the HS512 algorithm and secret key.
    //3. According to JWS Compact Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
    //   compaction of the JWT to a URL-safe string
    private String generateToken(Map<String, Object> claims, String subject) {
        final long now = System.currentTimeMillis();
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(now))
                .setExpiration(new Date(now + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    public Boolean validateToken(String token, String userFingerprintHash) {
        Claims claims = getAllClaimsFromToken(token);
            return claims.get("userFingerprint", String.class).equals(userFingerprintHash);
    }

    //validate token
    public Boolean validateToken(String token) {
        final String username = getEmailFromToken(token);
        return username != null && !isTokenExpired(token);
    }
}
