package com.example.jwt.services;

import com.example.jwt.data.CustomUserDetails;
import com.example.jwt.data.EditedImportData;
import com.example.jwt.data.ExportData;
import com.example.jwt.data.ImportData;
import com.example.jwt.exceptions.Exception400;
import com.example.jwt.exceptions.Exception500;
import com.example.jwt.model.QueryEntity;
import com.example.jwt.repository.QueryRepository;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.security.RolesAllowed;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Slf4j
@RolesAllowed("ROLE_USER")
public class ArticleService {

    @Autowired
    QueryRepository queryRepository;

    final static String subst1 = "<$1_$2>";
    final static String subst2 = "</$1_$2>";
    final static String replaceRegex1 = "<(\\w+)\\s+(\\w+)>";
    final static String replaceRegex2 = "<\\/(\\w+)\\s+(\\w+)>";

    public Object importArticle(ImportData importData) throws Exception400, Exception500 {
        String userID = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        //stage 1 is user submitting the raw bibtex
        if (importData.getStage() == 1) {
            List<Map<String, String>> parsedList = new ArrayList<>();
            try {
                //splitting the raw bibtex into multiple bibtexes if multiple bibtexes are provided and trim any whitespace
                Arrays.stream(importData.getBibtex().split("@\\w+\\{")).filter(str -> !str.isEmpty()).map(String::trim).forEach(bib -> {
                    //for each bibtex
                    //fix month in order to match the regex
                    bib = bib.replaceAll("month\\s=\\s*(\\w{3}|\\d{1,2})\\s*", "month = $1}");
                    //fix the year in order to match the regex
                    bib = bib.replaceAll("year\\s=\\s*(\\w{4})\\s*", "year = $1}");
                    Map<String, String> localMap = new HashMap<>();

                    //custom regex to parse bibtex into key-value pairs
                    final String regex = "\\s*(\\w*)\\s*=\\s*([a-z0-9\\.\\/\\:\\s\\-\\{\\}\\(\\)\\\\\\,\\&\\–\\‘\\’\\[\\]\\+\\'\\$\\\"\\~\\|\\;\\^\\%\\#]*),?\\s*}";
                    final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
                    final Matcher matcher = pattern.matcher(bib);
                    //go through all bibtex results
                    while (matcher.find()) {
                        //put key-values into the map by trimming and removing any unnecessary characters
                        localMap.put(matcher.group(1), matcher.group(2).replaceAll("\\{", "")
                                .replace("}", "")
                                .replace("\n", "")
                                .replace("\\\\textquotesingle", "'"));
                    }
                    //append the map to the list
                    parsedList.add(localMap);
                });
                //return the values to the user
                return mapMapToObject(parsedList);

            } catch (Exception e) {
                log.error(e.getMessage());
                throw new Exception500(e.getMessage());
            }

        }
        //stage 2 - user has checked the values and returned them to be saved
        else {
            //check if data is present
            if (importData.getEditedImportDataList().isEmpty()) {
                return "Edited list is empty";
            }
            //check if required fields are missing
            else if (!importData.validateEditedData()) {
                throw new Exception400("field", "Mandatory field is missing");
            }
            //everything is valid
            else {
                //retrieve all saved articles so far by the user from the database
                Optional<List<QueryEntity>> optionalQueryEntities = queryRepository.findByUserID(userID);
                //user has no entries -> save all provided data into the database without filtering
                if (optionalQueryEntities.get().isEmpty()) {
                    return queryRepository.saveAll(importData.getEditedImportDataList().stream().
                            map(e -> generateQueryEntity(e, userID)).collect(Collectors.toUnmodifiableList()));
                }
                //user has saved articles in the database
                else {

                    List<String> titles = optionalQueryEntities.get().stream().map(QueryEntity::getTitle).collect(Collectors.toUnmodifiableList());
                    //save only the articles that are not duplicated based on article title
                    return queryRepository.saveAll(importData.getEditedImportDataList().stream().
                            filter(entry -> !titles.contains(entry.getTitle())).
                            map(e -> generateQueryEntity(e, userID)).
                            collect(Collectors.toUnmodifiableList()));
                }
            }
        }
    }

    private QueryEntity generateQueryEntity(EditedImportData entry, String userID) {
        QueryEntity q = new QueryEntity();
        q.setUserID(userID);
        q.setAuthors(entry.getAuthors());
        q.setTitle(entry.getTitle());
        q.setVenue(entry.getMetadata().remove("venue"));
        q.setVolume(entry.getMetadata().remove("volume"));
        q.setNumber(entry.getMetadata().remove("number"));
        q.setPages(entry.getMetadata().remove("pages"));
        q.setYear(entry.getYear());
        q.setType(entry.getMetadata().remove("type"));
        q.setDoi(entry.getDoi());
        q.setEe(entry.getUrl());
        q.setUrl(entry.getUrl());
        q.setBooktitle(entry.getMetadata().remove("booktitle"));
        q.setPublisher(entry.getMetadata().remove("publisher"));
        q.setMetadata(entry.getMetadata());
        return q;
    }

    private List<EditedImportData> mapMapToObject(List<Map<String, String>> input) {
        List<EditedImportData> editedImportDataList = new ArrayList<>();
        input.stream().filter(map -> map.containsKey("author") && map.containsKey("title") &&
                map.containsKey("year")).forEach(map -> {
            Map<String, String> metadata = new HashMap<>();
            EditedImportData e = new EditedImportData();
            String author = map.remove("author").replaceAll("\\s+", " ");
            e.setAuthors(Arrays.stream(author.split(author.contains("and") ? " and " : ",")).map(String::trim).collect(Collectors.toUnmodifiableList()));
            e.setTitle(map.remove("title").replaceAll("\\s+", " "));
            e.setYear(map.remove("year"));
            e.setDoi(map.remove("doi"));
            e.setUrl(map.remove("url"));
            map.forEach(metadata::put);
            e.setMetadata(metadata);

            editedImportDataList.add(e);
        });
        return editedImportDataList;
    }

    public List<QueryEntity> getarticles() throws Exception400 {
        String userID = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        return queryRepository.findByUserID(userID).orElseThrow(() -> new Exception400("articles", "User has no articles"));
    }

    public Object export(ExportData exportData) throws Exception400, IOException {
        String userID = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        //create new string builder
        StringBuilder finalBibTex = new StringBuilder();

        //retrieve all database entries for the provided IDs
        List<QueryEntity> queryEntityList = queryRepository.findObjectsByObjectIDs(exportData.getArticleIDs().keySet(), userID).get();
        if (queryEntityList.size() == 0) throw new Exception400("Articles", "No Articles found");

        if (exportData.getExportTo().equalsIgnoreCase("bibtex")) {
            //iterate over the retrieved entries from the database
            for (QueryEntity entity : queryEntityList) {
                //split authors into an array of authors
                String[] author = entity.getAuthors().get(0).split(" ");
                //append the typical bibtex start
                finalBibTex.append("@").append(exportData.getArticleIDs().get(entity.get_id()))
                        .append("{").append((author[author.length - 1]).replace(" ", "")).append(entity.getYear()).append(",");

                //iterate over each entry's class's get methods
                Arrays.stream(entity.getClass().getMethods()).
                        //filter only data we want inside the bibtex
                        filter(m -> m.getName().contains("get") && !m.getName().contains("id") && !m.getName().contains("Metadata") && !m.getName().contains("Class") && !m.getName().contains("UserID") && !m.getName().contains("Author")).
                        forEach(m -> {
                            //for each getter method, invoke the method and append the data to the bibtex
                            try {
                                Object tempO = m.invoke(entity);
                                if (tempO != null)
                                    finalBibTex.append("\n\t").append(m.getName().replace("get", "").toLowerCase()).
                                            append(" = {").append(tempO).append("},");
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        });
                //append authors to the bibtex
                finalBibTex.append("\n\tauthor = {").append(String.join(" and ", entity.getAuthors())).append("},");
                //append any metadata to the bibtex
                if (entity.getMetadata() != null && !entity.getMetadata().isEmpty())
                    entity.getMetadata().forEach((k, v) -> finalBibTex.append("\n\t").append(k).append(" = {").append(v).append("}"));
                //finish the bibtex
                finalBibTex.append("\n}\n\n");
            }
            //return the bibtex string to the user
            return finalBibTex.toString();
        } else if (exportData.getExportTo().equalsIgnoreCase("csv")) {
            List<String> uselessFields = Arrays.asList("_id", "userID", "metadata");
            List<String> csvHeader = Arrays.stream(queryEntityList.get(0).getClass().getDeclaredFields())
                    .map(Field::getName).filter(name -> !uselessFields.contains(name)).collect(Collectors.toList());

            StringBuilder csvOutput = new StringBuilder();
            csvOutput.append(String.join(",", csvHeader)).append("\n");
            queryEntityList.forEach(q -> csvOutput.append("\"" + String.join(", ", q.getAuthors()) + "\",").append(appendIfNotNull("\"".concat(q.getTitle()).concat("\""),false))
                    .append(appendIfNotNull(q.getAbstract(), true))
                    .append(appendIfNotNull(q.getVenue(),false)).append(appendIfNotNull(q.getVolume(),false))
                    .append(appendIfNotNull(q.getNumber(),false)).append(appendIfNotNull(q.getPages(),false))
                    .append(appendIfNotNull(q.getYear(),false)).append(appendIfNotNull(q.getType(),false))
                    .append(appendIfNotNull(q.getKey(),false)).append(appendIfNotNull(q.getDoi(),false))
                    .append(appendIfNotNull(q.getEe(),false)).append(appendIfNotNull(q.getUrl(),false))
                    .append(appendIfNotNull(q.getPublisher(),false)).append(appendIfNotNull(q.getBooktitle(),false)).append("\n"));
            return csvOutput.toString();

        } else if (exportData.getExportTo().equalsIgnoreCase("xml")) {
            XmlMapper xmlMapper = new XmlMapper();
            String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
            return replaceXMLKeySpace(xml + xmlMapper.writeValueAsString(queryEntityList).replace("ArrayList", "articles").replace("item", "article"));


        } else throw new Exception400("format", "Unsupported Format");
    }

    private String replaceXMLKeySpace(String str) {
        final Pattern pattern1 = Pattern.compile(replaceRegex1, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
        final Pattern pattern2 = Pattern.compile(replaceRegex2, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
        final Matcher matcher1 = pattern1.matcher(str);
        str = matcher1.replaceAll(subst1);
        final Matcher matcher2 = pattern2.matcher(str);
        str = matcher2.replaceAll(subst2);
        return str;
    }

    String newline = System.getProperty("line.separator");

    private String appendIfNotNull(String s, boolean special) {
        if (s == null || s.trim().length() == 0) return " ,";
        else {
            if(s.contains("\"")) s = s.replace("\"", "\"\"");
            if(special)
                return "\"".concat(s).concat("\",");
            return s.concat(",");
        }
    }

    public String delete(String id) {
        queryRepository.deleteById(id);
        return "OK";
    }

    public QueryEntity modify(QueryEntity queryEntity, String id) throws Exception400, NoSuchMethodException {
        String userID = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        //retrieve the database entry for the given article ID
        QueryEntity queryEntity1 = queryRepository.findBy_idAndUserID(id, userID).orElseThrow(() -> new Exception400("article", "Not Found"));

        //get all the set fields for the class programmatically
        Field[] fields = queryEntity.getClass().getDeclaredFields();
        //iterate over the field
        for (Field f : fields) {
            String fieldname = f.getName();
            PropertyDescriptor pd1;
            PropertyDescriptor pd2;
            try {
                //programmatically get the getter for the field and invoke it
                pd1 = new PropertyDescriptor(fieldname, queryEntity.getClass());
                if (pd1.getReadMethod().invoke(queryEntity) != null) {
                    //programmatically get the setter for the field and invoke it
                    pd2 = new PropertyDescriptor(fieldname, queryEntity1.getClass());
                    pd2.getWriteMethod().invoke(queryEntity1, pd1.getReadMethod().invoke(queryEntity));
                }
            } catch (IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        //save the modified entry to the database
        return queryRepository.save(queryEntity1);
    }

    public Object add(QueryEntity queryEntity) throws Exception400 {
        String userID = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        Optional<QueryEntity> queryEntityOptional = queryRepository.findByTitleAndUserID(queryEntity.getTitle(), userID);
        if (queryEntityOptional.isPresent()) {
            throw new Exception400("article", "Article already exists");
        } else {
            queryEntity.setUserID(userID);
            return queryRepository.save(queryEntity);
        }
    }

    public QueryEntity getarticle(String id) throws Exception400 {
        return queryRepository.findById(id).orElseThrow(() -> new Exception400("article", "Article not found"));
    }
}
