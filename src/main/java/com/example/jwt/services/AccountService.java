package com.example.jwt.services;

import com.example.jwt.data.*;
import com.example.jwt.exceptions.Exception400;
import com.example.jwt.exceptions.Exception500;
import com.example.jwt.model.UserEntity;
import com.example.jwt.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.jboss.aerogear.security.otp.Totp;
import org.jboss.aerogear.security.otp.api.Base32;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.security.RolesAllowed;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

@Service
@Slf4j
public class AccountService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    SendEmailService sendEmailService;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    Random random;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenService jwtTokenUtil;

    @Autowired
    SendEmailService sendEmail;



    public final String QR_PREFIX =
            "https://chart.googleapis.com/chart?chs=200x200&chld=M%%7C0&cht=qr&chl=";


    private final String mendeleyLoginURL = "https://api.mendeley.com/oauth/authorize?client_id=9055&redirect_uri=https://api.publicationlistmanager.me/account/mendeley&response_type=code&scope=all&state={{userID}}";


    @Autowired
    public AccountService(RestTemplateBuilder restTemplateBuilder) {

    }

    public String[] login(LoginData authenticationRequest) throws Exception {
        Authentication auth = springAuth(authenticationRequest.getEmail(), authenticationRequest.getPassword());
        SecurityContextHolder.getContext().setAuthentication(auth);
        return (jwtTokenUtil.generateToken(auth));
    }

    private Authentication springAuth(String username, String password) throws Exception {
        try {
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    public String register(RegisterData user) throws Exception {
        if (userRepository.findByEmail(user.getEmail()).isPresent()) {
            throw new Exception400("email", "Account with this email already exists");
        }
        if (!user.getPassword().equals(user.getRepeatPassword())) {
            throw new Exception400("password", "Passwords do not match");
        }
        UserEntity newUser = new UserEntity();
        newUser.setEmail(user.getEmail());
        newUser.setFirstname(user.getFirstName());
        newUser.setLastname(user.getLastName());
        newUser.setSecret(Base32.random());
        newUser.setActivated(false);
        newUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        newUser.setActivationUUID(UUID.randomUUID().toString());

        try {
            userRepository.save(newUser);
            sendEmailService.verifyEmail(newUser.getEmail(), newUser.getActivationUUID());
            return "Registered";
        } catch (Exception e) {
            throw new Exception500(e.getMessage());
        }
    }

    public String activate(String uuid) throws Exception400 {
        UserEntity userEntity = userRepository.findByActivationUUID(uuid).orElseThrow(() -> new Exception400("uuid", "Wrong UUID"));
        userEntity.setActivationUUID("");
        userEntity.setActivated(true);
        userRepository.save(userEntity);
        return "Activated";
    }

    public String twoFactorAuth(TwoFactorData twoFactorData) throws Exception400 {
        UserEntity userEntity = userRepository.findByActivationUUID(twoFactorData.getUuid()).orElseThrow(() -> new Exception400("uuid", "Wrong UUID"));

        Totp totp = new Totp(userEntity.getSecret());
        if (!isValidLong(twoFactorData.getCode()) || !totp.verify(twoFactorData.getCode())) {
            throw new Exception400("code", "Bad Code");
        } else {
            userEntity.setActivated(true);
            userEntity.setActivationUUID("");
            userEntity.setTwoFactorAuth(true);
            userRepository.save(userEntity);
            return "OK";
        }
    }

    private boolean isValidLong(String code) {
        try {
            Long.parseLong(code);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public Map<String, String> generateQR(String uuid) throws Exception {
        UserEntity userEntity = userRepository.findByActivationUUID(uuid).orElseThrow(() -> new Exception400("uuid", "Wrong UUID"));
        return generateQRCode(userEntity);
    }

    public String changePassword(ChangePasswordData changePasswordData) throws Exception400 {
        if (!changePasswordData.checkPasswords()) throw new Exception400("password", "New passwords are not the same");
        String userID = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        UserEntity user = userRepository.findById(userID).get();
        if (bCryptPasswordEncoder.matches(changePasswordData.getPassword(), user.getPassword())) {
            user.setPassword(bCryptPasswordEncoder.encode(changePasswordData.getNewPassword()));
            userRepository.save(user);
            return "Changed";
        } else throw new Exception400("password", "Wrong password");
    }

    public String resetPassword(PasswordResetBody passwordResetBody) throws Exception400 {
        UserEntity user = userRepository.findByEmail(passwordResetBody.getEmail()).orElse(null);
        if (user != null) {
            if (passwordResetBody.checkUUID()) {
                if (!passwordResetBody.validatePassword())
                    throw new Exception400("password", "New passwords are not the same");
                if (!passwordResetBody.getUuid().equals(user.getResetUUID())) {
                    throw new Exception400("uuid", "Invalid UUID");
                }
                user.setPassword(bCryptPasswordEncoder.encode(passwordResetBody.getPassword()));
                userRepository.save(user);
                return "Reset";
            } else {
                user.setResetUUID(UUID.randomUUID().toString());
                return "Email";
            }
        } else {
            return "Email";
        }
    }

    public String postPrompt() throws Exception400 {
        String userID = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        UserEntity user = userRepository.findById(userID).orElseThrow(() -> new Exception400("user", "User not Found"));
        user.setConfirmProviders(!user.getConfirmProviders());
        userRepository.save(user);
        return "OK";
    }

    @RolesAllowed("ROLE_USER")
    public Map<String, String> getProviders() throws Exception400 {
        Map<String, String> returnMap = new HashMap<>();
        String userID = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        UserEntity user = userRepository.findById(userID).orElseThrow(() -> new Exception400("user", "User not Found"));
        if (!user.getConfirmProviders()) {
//            if (!user.getMendeleyEnabled()) {
//                returnMap.put("Mendeley", mendeleyLoginURL.replace("https://api.publicationlistmanager.me", redirectURL).replace("{{userID}}", userID));
//            }
            return returnMap;
        } else {
            returnMap.put("Providers", "none");
            return returnMap;
        }
    }

    private Map<String, String> generateQRCode(UserEntity user) {
        Map<String, String> map = new HashMap<>();
        map.put("qrcode", QR_PREFIX + URLEncoder.encode(String.format(
                "otpauth://totp/%s:%s?secret=%s&issuer=%s",
                "Publication List Manager", user.getEmail().trim(), user.getSecret().trim(), "Publication List Manager"),
                StandardCharsets.UTF_8));

        map.put("secret", user.getSecret().trim().substring(0, 4) + " " + user.getSecret().trim().substring(4, 8) + " " +
                user.getSecret().trim().substring(8, 12) + " " + user.getSecret().trim().substring(12, 16));
        return map;
    }

    @RolesAllowed("ROLE_USER")
    public Map<String, Boolean> is2FAEnabled() throws Exception400 {
        String userID = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        UserEntity user = userRepository.findById(userID).orElseThrow(() -> new Exception400("user", "User not Found"));
        return new HashMap<>(){{
            put("enabled", user.isTwoFactorAuth());
        }};
    }
}
