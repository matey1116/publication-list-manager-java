package com.example.jwt.services;

import com.example.jwt.config.Constants;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SendEmailService {
    private final Email from = new Email("support@publicationlistmanager.me");
    @Autowired
    SendGrid sendGrid;

    public void verifyEmail(String toString, String link) throws IOException{
        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");

        Content content = new Content("text/html", Constants.verifyEmail.replaceAll("\\{\\{link}}", "https://publicationlistmanager.me/activateAccount/" + link));

        Email to = new Email(toString);
        Mail mail = new Mail(from, "Email Verification", to, content);


        request.setBody(mail.build());
        sendGrid.api(request);


    }

}
