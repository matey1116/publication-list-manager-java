package com.example.jwt.exceptions;

public class Exception500 extends Exception {
    public Exception500(String error){
        super(error);
    }
}
