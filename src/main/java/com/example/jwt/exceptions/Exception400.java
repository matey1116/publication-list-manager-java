package com.example.jwt.exceptions;

import lombok.Getter;

import java.util.AbstractMap;

public class Exception400 extends Exception {
    @Getter
    AbstractMap.SimpleImmutableEntry<String, String> simpleImmutableEntry;
    public Exception400(String field, String message){
        super(field);
        simpleImmutableEntry = new AbstractMap.SimpleImmutableEntry<>(field, message);
    }
}
