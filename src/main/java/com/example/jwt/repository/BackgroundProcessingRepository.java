package com.example.jwt.repository;

import com.example.jwt.model.BackgroundProcessing;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface BackgroundProcessingRepository extends MongoRepository<BackgroundProcessing, String> {
    Optional<BackgroundProcessing> findByUuid(String uuid);
}
