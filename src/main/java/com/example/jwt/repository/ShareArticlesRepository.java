package com.example.jwt.repository;

import com.example.jwt.model.ShareArticlesEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ShareArticlesRepository extends MongoRepository<ShareArticlesEntity, String> {
    @Query(value = "{articles: ?0, 'userID': ?1}") //{'$in': ?0}
    Optional<ShareArticlesEntity> findArticles(List<String> articles, String userID);

}
