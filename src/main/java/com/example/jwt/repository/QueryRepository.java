package com.example.jwt.repository;

import com.example.jwt.model.QueryEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface QueryRepository extends MongoRepository<QueryEntity, String> {
    Optional<QueryEntity> findByTitleAndUserID(String title, String userID);

    @Query(value = "{ 'authors' : {$all : [?0] }, 'userID': ?1}")
    Optional<List<QueryEntity>> findByAnyAuthorAndUserID(String author, String userID);

    Optional<List<QueryEntity>> findByUserID(String userID);

    @Query(value = "{'userID' : ?0}", fields = "{_id: 1}")
    Optional<List<QueryEntity>> findByUserIDOnlyObjectId(String userID);

    @Query(value = "{_id: {'$in': ?0}, 'userID': ?1}")
    Optional<List<QueryEntity>> findObjectsByObjectIDs(Set<String> ids, String userID);

    @Query(value = "{_id: {'$in': ?0}}")
    Optional<List<QueryEntity>> findByQueryIds(List<String> id);

    Optional<QueryEntity> findBy_idAndUserID(String id, String userID);

//    Optional<QueryEntity> findByTitleAndUserID(String title, String userID);
}
