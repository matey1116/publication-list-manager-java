package com.example.jwt.repository;

import com.example.jwt.model.RateLimitMongo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RateLimitMongoRepository extends MongoRepository<RateLimitMongo, String> {
}
