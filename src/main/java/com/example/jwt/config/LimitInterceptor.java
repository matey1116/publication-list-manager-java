package com.example.jwt.config;

import com.example.jwt.data.CustomUserDetails;
import com.example.jwt.services.UserLimiter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class LimitInterceptor implements HandlerInterceptor {

    @Autowired
    UserLimiter userLimiter;

    @Override
    public boolean preHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (request.getMethod().equals("OPTIONS"))
            return true;
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (userLimiter.checkLimit(user.getId())) return true;
        else {
            response.sendError(429);
            return false;
        }
    }
}
