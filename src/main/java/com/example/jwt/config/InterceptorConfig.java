package com.example.jwt.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Component
public class InterceptorConfig implements WebMvcConfigurer {

    final LimitInterceptor limitInterceptor;

    @Autowired
    public InterceptorConfig(LimitInterceptor limitInterceptor) {
        this.limitInterceptor = limitInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(limitInterceptor).addPathPatterns("/hello");

        // multiple urls (same is possible for `exludePathPatterns`)
//        registry.addInterceptor(new SecurityInterceptor()).addPathPatterns("/secure/*", "/admin/**", "/profile/**");
    }

}
