package com.example.jwt.config;

import com.example.jwt.data.CustomUserDetails;
import com.example.jwt.services.JwtTokenService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.Optional;

@Component
@Slf4j
public class JwtRequestFilter extends OncePerRequestFilter {
    public static final String AUTHORIZATION = "Authorization";
    public static final String BEARER = "Bearer ";
    private final JwtTokenService jwtTokenService;

    @Autowired
    public JwtRequestFilter(JwtTokenService jwtTokenService) {
        this.jwtTokenService = jwtTokenService;
    }

    @Autowired
    MessageDigest messageDigest;

    @SneakyThrows
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException {
//        String userFingerprint = null;
//        if (request.getCookies() != null && request.getCookies().length > 0) {
//            List<Cookie> cookies = Arrays.stream(request.getCookies()).collect(Collectors.toList());
//            Optional<Cookie> cookie = cookies.stream().filter(c -> "__Secure-Fgp"
//                    .equals(c.getName())).findFirst();
//            if (cookie.isPresent()) {
//                userFingerprint = cookie.get().getValue();
//            }
//        }
//
//        MessageDigest digest = MessageDigest.getInstance("SHA-256");
//        byte[] userFingerprintDigest = digest.digest(userFingerprint.getBytes(StandardCharsets.UTF_8));
//        String userFingerprintHash = DatatypeConverter.printHexBinary(userFingerprintDigest);


        final Optional<String> jwt = getJwtFromRequest(request);
        jwt.ifPresent(token -> {
            try {
//                if (jwtTokenService.validateToken(token, userFingerprintHash)) {
                if (jwtTokenService.validateToken(token)) {
                    setSecurityContext(new WebAuthenticationDetailsSource().buildDetails(request), token);
                } else {
                    throw new MalformedJwtException("Invalid Token");
                }
            } catch (IllegalArgumentException | MalformedJwtException | ExpiredJwtException e) {
                logger.error(e);
                logger.error("Unable to get JWT Token or JWT Token has expired");
            }
        });
        filterChain.doFilter(request, response);
    }

    private void setSecurityContext(WebAuthenticationDetails authDetails, String token) {
        final String email = jwtTokenService.getEmailFromToken(token);
        final Claims claims = jwtTokenService.getAllClaimsFromToken(token);
        final CustomUserDetails userDetails = new CustomUserDetails(claims.get("id", String.class), email, "", claims.get("firstName", String.class), claims.get("lastName", String.class), claims.get("access", Integer.class), getAuthorities(claims.get("access", Integer.class)));
        final UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null,
                userDetails.getAuthorities());
        authentication.setDetails(authDetails);
        // After setting the Authentication in the context, we specify
        // that the current user is authenticated. So it passes the
        // Spring Security Configurations successfully.
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private static Optional<String> getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader(AUTHORIZATION);
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(BEARER)) {
            return Optional.of(bearerToken.substring(7));
        }
        return Optional.empty();
    }

    private Collection<GrantedAuthority> getAuthorities(Integer access) {
        if (access == 1) {
            return AuthorityUtils.createAuthorityList("ROLE_ADMIN", "ROLE_USER");
        } else
            return AuthorityUtils.createAuthorityList("ROLE_USER");
    }
}
