package com.example.jwt.data;

import lombok.Data;

@Data
public class Enable2FAData {
    private int stage;
    private boolean enable;
    private String password;
    private String code;

    public boolean getEnable(){
        return enable;
    }
}
