package com.example.jwt.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ChangePasswordData {
    @NotBlank(message = "Mandatory field missing")
    private String password;
    @NotBlank(message = "Mandatory field missing")
    private String newPassword;
    @NotBlank(message = "Mandatory field missing")
    private String repeatNewPassword;

    public boolean checkPasswords(){
        return repeatNewPassword.equals(newPassword);
    }
}
