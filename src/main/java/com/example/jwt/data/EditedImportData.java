package com.example.jwt.data;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class EditedImportData {
    private String title;
    private List<String> authors;
    private String year;
    private String doi;
    private String url;
    private Map<String, String> metadata;
}
