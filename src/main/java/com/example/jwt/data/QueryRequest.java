package com.example.jwt.data;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class QueryRequest {
    @NotEmpty
    private String name;
    @NotEmpty
    private String type;
//    @NotNull
    private boolean searchOwnRecord;

    @NotNull
    @Pattern(regexp = "^true$|^false$", message = "allowed input: true or false")
    private String abstracts;

//    @NotNull
    private boolean liveFetch;

    public boolean getLiveFetch(){
        return liveFetch;
    }

    public boolean getAbstracts(){
        return Boolean.parseBoolean(abstracts);
    }

    public boolean queryAuthor(){
        return type.equalsIgnoreCase("author");
    }
    public boolean isOwnRecord(){
        return searchOwnRecord;
    }
}
