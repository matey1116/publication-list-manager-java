package com.example.jwt.data;

import lombok.Data;

@Data
public class ResetPasswordData {
    private String email;
    private String uuid;
    private String newPassword;
    private String repeatPassword;
}
