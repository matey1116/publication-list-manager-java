package com.example.jwt.data.mendeley;

import lombok.Data;

import javax.xml.bind.annotation.XmlElement;

@Data
public class MendeleyEntry {
    private String title;
    private String year;
    private String source;
    @XmlElement(name = "abstract")
    private String abs;
}
