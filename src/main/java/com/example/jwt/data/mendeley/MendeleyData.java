package com.example.jwt.data.mendeley;

import lombok.Data;

import java.util.List;

@Data
public class MendeleyData {
    private List<MendeleyEntry> mendeleyEntryList;
}
