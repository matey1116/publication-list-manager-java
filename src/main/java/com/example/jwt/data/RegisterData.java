package com.example.jwt.data;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
public class RegisterData {
    @NotEmpty
    @Email(message = "Invalid email")
    private String email;
    @NotEmpty(message = "First Name is required")
    private String firstName;
    @NotEmpty(message = "Last Name is required")
    private String lastName;
    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$", message = "Password must be 8 characters minimum and must contain 1 lower, 1 upper and 1 number")
    private String password;
    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$", message = "Password must be 8 characters minimum and must contain 1 lower, 1 upper and 1 number")
    private String repeatPassword;
}
