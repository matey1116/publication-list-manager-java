package com.example.jwt.data;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class TwoFactorData {
    @NotEmpty(message = "UUID must not be empty")
    private String uuid;
    @NotEmpty(message = "Code must not be empty")
    private String code;
}
