package com.example.jwt.data;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class LoginData {
    @Email(message = "Invalid Email")
    private String email;
    @NotEmpty(message = "Password must not be empty")
    @Length(min = 6, message = "Minimum password is 6")
    private String password;

    private String token;
}
