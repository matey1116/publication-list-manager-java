package com.example.jwt.data;

import lombok.Getter;
import lombok.Setter;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

@Getter
@Setter
public class CustomUserDetails extends User {
    private String email;
    private long access;
    private String id;
    private String firstName;
    private String lastName;
    private Collection<GrantedAuthority> authorities;
    public CustomUserDetails(String id, String email, String password, String firstName, String lastName, int access, Collection<GrantedAuthority> authorities){
        super(email, password, true, true, true, true, authorities);
        this.email = email;
        this.access = access;
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.authorities = authorities;
    }
}
