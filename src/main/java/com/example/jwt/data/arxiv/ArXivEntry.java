package com.example.jwt.data.arxiv;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.Data;

import java.util.List;

@Data
public class ArXivEntry {
    private String id;
    private String title;
    private String summary;
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<List<String>> author;
    private String doi;

}
