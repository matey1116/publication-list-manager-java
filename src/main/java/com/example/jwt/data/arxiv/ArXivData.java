package com.example.jwt.data.arxiv;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.Data;

import java.util.List;

@Data
public class ArXivData {
    @JacksonXmlElementWrapper(useWrapping = false)
    public List<ArXivEntry> entry;
}
