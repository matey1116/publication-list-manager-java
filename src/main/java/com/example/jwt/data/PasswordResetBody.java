package com.example.jwt.data;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class PasswordResetBody {
    @NotEmpty(message = "Email is required")
    private String email;
    private String password;
    private String repeatPassword;
    private String uuid;

    public boolean checkUUID(){
        return uuid != null;
    }

    public boolean validatePassword(){
        return password.equals(repeatPassword);
    }
}
