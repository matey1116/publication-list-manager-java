package com.example.jwt.data;

import lombok.Data;

import java.util.Map;

@Data
public class ExportData {
    private Map<String, String> articleIDs;
    private String exportTo;
    private String exportRaw;
}
