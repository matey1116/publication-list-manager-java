package com.example.jwt.data;

import lombok.Data;

import java.util.List;

@Data
public class ImportData {
    private int stage;
    private String bibtex;

    List<EditedImportData> editedImportDataList;

    public boolean validateEditedData(){
        return editedImportDataList.stream().allMatch(eid -> eid.getTitle() != null && !eid.getTitle().trim().isEmpty() &&
                eid.getAuthors().size() != 0 &&
                eid.getUrl() != null && !eid.getUrl().trim().isEmpty() &&
                eid.getYear() != null && !eid.getYear().trim().isEmpty());
    }
}
