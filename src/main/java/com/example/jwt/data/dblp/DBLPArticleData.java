package com.example.jwt.data.dblp;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class DBLPArticleData {
    private List<String> authors;
    private String Abstract;
    private String title;
    private String venue;
    private String volume;
    private String number;
    private String pages;
    private String year;
    private String type;
    private String key;
    private String doi;
    private String ee;
    private String url;
}
