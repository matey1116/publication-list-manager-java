package com.example.jwt.data.dblp;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.Getter;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Getter
@XmlRootElement(name = "result")
public class DBLPRawData {
    @JacksonXmlElementWrapper(localName = "hits")
    private List<DBLPHit> hits;
}


