package com.example.jwt.data.dblp;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "info")
public class DBLPHit {
    private DBLPArticleData info;
}
