package com.example.jwt.data;

import com.example.jwt.data.dblp.DBLPArticleData;
import lombok.Data;

import java.util.List;

@Data
public class QueryResponse {
    private List<DBLPArticleData> dblpArticles;
}
